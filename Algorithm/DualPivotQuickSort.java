// Java program to implement
// dual pivot QuickSort
import java.io.*;
class DualPivotQuickSort{

	static void swap(int[] arr, int i, int j)
	{
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	static void dualPivotQuickSort(int[] arr,
								int low, int high)
	{
		if (low < high)
		{
			
			// piv[] stores left pivot and right pivot.
			// piv[0] means left pivot and
			// piv[1] means right pivot
			int[] piv;
			piv = partition(arr, low, high);
			
			dualPivotQuickSort(arr, low, piv[0] - 1);
			dualPivotQuickSort(arr, piv[0] + 1, piv[1] - 1);
			dualPivotQuickSort(arr, piv[1] + 1, high);
		}
	}

	static int[] partition(int[] arr, int low, int high)
	{
		if (arr[low] > arr[high])
			swap(arr, low, high);
			
		// p is the left pivot, and q
		// is the right pivot.
		int j = low + 1;
		int g = high - 1, k = low + 1,
			p = arr[low], q = arr[high];
			
		while (k <= g)
		{
			
			// If elements are less than the left pivot
			if (arr[k] < p)
			{
				swap(arr, k, j);
				j++;
			}
			
			// If elements are greater than or equal
			// to the right pivot
			else if (arr[k] >= q)
			{
				while (arr[g] > q && k < g)
					g--;
					
				swap(arr, k, g);
				g--;
				
				if (arr[k] < p)
				{
					swap(arr, k, j);
					j++;
				}
			}
			k++;
		}
		j--;
		g++;
		
		// Bring pivots to their appropriate positions.
		swap(arr, low, j);
		swap(arr, high, g);

		// Returning the indices of the pivots
		// because we cannot return two elements
		// from a function, we do that using an array.
		return new int[] { j, g };
	}

// Driver code
	public static void main(String[] args)
	{	
		File input = new File("D:\\semester 4\\daa\\paper\\sort\\git\\Testcase\\test1000of100000.txt");
		
		try(BufferedReader br = new BufferedReader(new FileReader(input))){
			FileWriter writer = new FileWriter("dualpivot1000x100000.csv");
			String st;

			st = br.readLine();
			int jumlahTestcase = Integer.parseInt(st);

			double totalTime=0;

	  		while ((st = br.readLine()) != null){
				int jumlahAngka = Integer.parseInt(st);

				st = br.readLine();
	   			String[] integersInString = st.split(" ");
				int[] arr = new int[integersInString.length];
				for (int i = 0; i < integersInString.length; i++) {
				    arr[i] = Integer.parseInt(integersInString[i]);
				}

				long startTime = System.nanoTime();
				dualPivotQuickSort(arr, 0, jumlahAngka-1);
				long stopTime = System.nanoTime();
				long elapsedTime = stopTime-startTime;
				double elapsedTimeInSecond = (double) elapsedTime / 1000000000;
				totalTime+=elapsedTimeInSecond;
				System.out.println(elapsedTimeInSecond);
				writer.write(""+elapsedTimeInSecond+System.getProperty( "line.separator" ));
	  		}
				System.out.println(totalTime);
				writer.flush();
				writer.close();
  		}
		catch(IOException e){
			e.printStackTrace();
		}
					
	}
}


// taken and modified from https://www.geeksforgeeks.org/dual-pivot-quicksort/
// This code is contributed by Gourish Sadhu


