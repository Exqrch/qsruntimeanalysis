import java.util.Arrays;
import java.io.*;

class HoareQsort
{
    public static void swap (int[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
 
    // Partition using Hoare's Partitioning scheme
    public static int partition(int[] a, int low, int high)
    {
        int pivot = a[(low+high)/2];
        int i = low - 1;
        int j = high + 1;
 
        while (true)
        {
            do {
                i++;
            } while (a[i] < pivot);
 
            do {
                j--;
            } while (a[j] > pivot);
 
            if (i >= j) {
                return j;
            }
 
            swap(a, i, j);
        }
    }
 
    // Quicksort routine
    public static void quicksort(int[] a, int low, int high)
    {
        // base condition
        if (low >= high) {
            return;
        }
 
        // rearrange elements across pivot
        int pivot = partition(a, low, high);
 
        // recur on subarray containing elements less than the pivot
        quicksort(a, low, pivot);
 
        // recur on subarray containing elements more than the pivot
        quicksort(a, pivot + 1, high);
    }
 
    public static void main(String[] args)
    {
        File input = new File("D:\\semester 4\\daa\\paper\\sort\\git\\Testcase\\test1000of100000.txt");
		
		try(BufferedReader br = new BufferedReader(new FileReader(input))){
			FileWriter writer = new FileWriter("hoare1000x100000.csv");
			String st;

			st = br.readLine();
			int jumlahTestcase = Integer.parseInt(st);

			double totalTime=0;

	  		while ((st = br.readLine()) != null){
				int jumlahAngka = Integer.parseInt(st);

				st = br.readLine();
	   			String[] integersInString = st.split(" ");
				int[] arr = new int[integersInString.length];
				for (int i = 0; i < integersInString.length; i++) {
				    arr[i] = Integer.parseInt(integersInString[i]);
				}

				long startTime = System.nanoTime();
				quicksort(arr, 0, jumlahAngka-1);
				long stopTime = System.nanoTime();
				long elapsedTime = stopTime-startTime;
				double elapsedTimeInSecond = (double) elapsedTime / 1000000000;
				totalTime+=elapsedTimeInSecond;
				System.out.println(elapsedTimeInSecond);
				writer.write(""+elapsedTimeInSecond+System.getProperty( "line.separator" ));
	  		}
				System.out.println(totalTime);
				writer.flush();
				writer.close();
  		}
		catch(IOException e){
			e.printStackTrace();
		}
    }
}

//taken and modified from https://www.techiedelight.com/quick-sort-using-hoares-partitioning-scheme/