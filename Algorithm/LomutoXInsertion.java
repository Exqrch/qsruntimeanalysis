// Java implementation QuickSort
// using Lomuto's partition Scheme
import java.io.*;

class LomutoXInsertion {
	static void Swap(int[] array, int position1, int position2)
	{
		// Swaps elements in an array
		
		// Copy the first position's element
		int temp = array[position1];
		
		// Assign to the second element
		array[position1] = array[position2];
		
		// Assign to the first element
		array[position2] = temp;
	}

	/* This function takes last element as
	pivot, places the pivot element at its
	correct position in sorted array, and
	places all smaller (smaller than pivot)
	to left of pivot and all greater elements
	to right of pivot */
	static int partition(int []arr, int low, int high)
	{
		int pivot = arr[high];
		
		// Index of smaller element
		int i = (low - 1);

		for (int j = low; j <= high- 1; j++)
		{
			// If current element is smaller
			// than or equal to pivot
			if (arr[j] <= pivot)
			{
				i++; // increment index of
					// smaller element
				Swap(arr, i, j);
			}
		}
		Swap(arr, i + 1, high);
		return (i + 1);
	}

	/* The main function that
	implements QuickSort
	arr[] --> Array to be sorted,
	low --> Starting index,
	high --> Ending index */
	static void quickSort(int []arr, int low,int high)
	{
		if (low < high)
		{
			if((high-low)>14){
				/* pi is partitioning index,
				arr[p] is now at right place */
				int pi = partition(arr, low, high);

				// Separately sort elements before
				// partition and after partition
				quickSort(arr, low, pi - 1);
				quickSort(arr, pi + 1, high);
			}else{
				insertionsort(arr, low, high);
			}
		}
	}

		static void insertionsort(int arr[], int low, int high)
    {
        for (int i = low+1; i <= high; i++) {
            int key = arr[i];
            int j = i - 1;
 
            /* Move elements of arr[0..i-1], that are
               greater than key, to one position ahead
               of their current position */
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }

// Driver Code
	static public void main (String[] args)
	{
		File input = new File("D:\\semester 4\\daa\\paper\\sort\\test1000of100000.txt");
		
		try(BufferedReader br = new BufferedReader(new FileReader(input))){
			FileWriter writer = new FileWriter("lomutox14insertion1000x100000.csv");
			String st;

			st = br.readLine();
			int jumlahTestcase = Integer.parseInt(st);

			double totalTime=0;

	  		while ((st = br.readLine()) != null){
				int jumlahAngka = Integer.parseInt(st);

				st = br.readLine();
	   			String[] integersInString = st.split(" ");
				int[] arr = new int[integersInString.length];
				for (int i = 0; i < integersInString.length; i++) {
				    arr[i] = Integer.parseInt(integersInString[i]);
				}

				long startTime = System.nanoTime();
				quickSort(arr, 0, jumlahAngka-1);
				long stopTime = System.nanoTime();
				long elapsedTime = stopTime-startTime;
				double elapsedTimeInSecond = (double) elapsedTime / 1000000000;
				totalTime+=elapsedTimeInSecond;
				System.out.println(elapsedTimeInSecond);
				writer.write(""+elapsedTimeInSecond+System.getProperty( "line.separator" ));
	  		}
				System.out.println(totalTime);
				writer.flush();
				writer.close();
  		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}

// taken and modified from https://www.geeksforgeeks.org/hoares-vs-lomuto-partition-scheme-quicksort/
// This code is contributed by vt_m.
