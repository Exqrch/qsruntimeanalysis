// Java program to illustrate
// Randomised Quick Sort
import java.util.*;
import java.io.*;

class RandomXInsertion
{	
	// This Function helps in calculating
	// random numbers between low(inclusive)
	// and high(inclusive)
	static void random(int arr[],int low,int high)
	{
	
		Random rand= new Random();
		int pivot = rand.nextInt(high-low)+low;
		
		int temp1=arr[pivot];
		arr[pivot]=arr[high];
		arr[high]=temp1;
	}
	
	/* This function takes last element as pivot,
	places the pivot element at its correct
	position in sorted array, and places all
	smaller (smaller than pivot) to left of
	pivot and all greater elements to right
	of pivot */
	static int partition(int arr[], int low, int high)
	{
		// pivot is choosen randomly
		random(arr,low,high);
		int pivot = arr[high];
	

		int i = (low-1); // index of smaller element
		for (int j = low; j < high; j++)
		{
			// If current element is smaller than or
			// equal to pivot
			if (arr[j] < pivot)
			{
				i++;

				// swap arr[i] and arr[j]
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}

		// swap arr[i+1] and arr[high] (or pivot)
		int temp = arr[i+1];
		arr[i+1] = arr[high];
		arr[high] = temp;

		return i+1;
	}


	/* The main function that implements QuickSort()
	arr[] --> Array to be sorted,
	low --> Starting index,
	high --> Ending index */
	static void sort(int arr[], int low, int high)
	{
		if (low < high)
		{
			if((high-low)>16){
				/* pi is partitioning index, arr[pi] is
				now at right place */
				int pi = partition(arr, low, high);

				// Recursively sort elements before
				// partition and after partition
				sort(arr, low, pi-1);
				sort(arr, pi+1, high);
			}else{
				insertionsort(arr, low, high);
			}
		}
	}

	static void insertionsort(int arr[], int low, int high)
    {
        for (int i = low+1; i <= high; i++) {
            int key = arr[i];
            int j = i - 1;
 
            /* Move elements of arr[0..i-1], that are
               greater than key, to one position ahead
               of their current position */
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }

	/* A utility function to print array of size n */
	static void printArray(int arr[])
	{
		int n = arr.length;
		for (int i = 0; i < n; ++i)
			System.out.print(arr[i]+" ");
		System.out.println();
	}

	// Driver Code
	public static void main(String args[])
	{
		File input = new File("D:\\semester 4\\daa\\paper\\sort\\git\\Testcase\\test1000of100000.txt");
		
		try(BufferedReader br = new BufferedReader(new FileReader(input))){
			FileWriter writer = new FileWriter("randomxinsertion1000x100000.csv");
			String st;

			st = br.readLine();
			int jumlahTestcase = Integer.parseInt(st);

			double totalTime=0;

	  		while ((st = br.readLine()) != null){
				int jumlahAngka = Integer.parseInt(st);

				st = br.readLine();
	   			String[] integersInString = st.split(" ");
				int[] arr = new int[integersInString.length];
				for (int i = 0; i < integersInString.length; i++) {
				    arr[i] = Integer.parseInt(integersInString[i]);
				}

				long startTime = System.nanoTime();
				sort(arr, 0, jumlahAngka-1);
				long stopTime = System.nanoTime();
				long elapsedTime = stopTime-startTime;
				double elapsedTimeInSecond = (double) elapsedTime / 1000000000;
				totalTime+=elapsedTimeInSecond;
				System.out.println(elapsedTimeInSecond);
				writer.write(""+elapsedTimeInSecond+System.getProperty( "line.separator" ));
	  		}
				System.out.println(totalTime);
				writer.flush();
				writer.close();
  		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
//taken and modified from https://www.geeksforgeeks.org/quicksort-using-random-pivoting/
// This code is contributed by Ritika Gupta.
